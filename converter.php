<?php

header("Content-type: text/xml");

class dataConverter
{
	
	public $data = ""; 
	public $xmlDocument;
	
	
	// Autoloader
	public function __construct() {
		
		if(file_exists('old_data.txt')){
			$this->data = file_get_contents('old_data.txt');
			$this->converter();
		}
		
	}
	
	
	// Catch data from file and split persons in pieces
	private function persons(){
		
		$data = $this->data;
		$personsData = preg_split('@(?=P\|)@', $data, 0, PREG_SPLIT_NO_EMPTY);
		foreach($personsData as $personData){
			$personsArray[] = preg_split('@(?=F\|)@', $personData, 0, PREG_SPLIT_NO_EMPTY);
		}
		return $personsArray;
		
	}
	
	
	// Parse array from pipe separated string
	private function lineParser( $keys, $array ){
		
		$line = array();
		foreach($keys as $id => $key){
			if(isset($array[$id]) && $array[$id]){
				$line[$key] = trim($array[$id]);
			}
		}
		return $line;
		
	}
	
	
	// Add element
	private function addElement($element, $appendTo){
		
		$element = $this->xmlDocument->createElement($element);
		$element = $appendTo->appendChild($element);
		
		return $element;
		
	}
	
	
	// Add element and text node
	private function addElementTextNode($element, $appendTo, $textNode){
			
		$child = $this->addElement($element, $appendTo);
		
		$value = $this->xmlDocument->createTextNode($textNode);
		$value = $child->appendChild($value);
		
		return $child;
		
	}
	
	
	// Change sort order to place address before phone
	private function sortOrder($rows){
		
		$sorted = array();
		foreach($rows as $row){
			if(substr($row,0,1) == 'P' || substr($row,0,1) == 'F'){
				$sorted['A'] = $row;
			}else if(substr($row,0,1) == 'A'){
				$sorted['B'] = $row;
			}else if(substr($row,0,1) == 'T'){
				$sorted['C'] = $row;
			}
		}
		ksort($sorted);
		
		return $sorted;
		
	}
	
	
	// Converter
	public function converter(){
		
		// Create XML document
		$this->xmlDocument = new DomDocument('1.0');
		
		// Add XML settings 
		$this->xmlDocument->preserveWhiteSpace = false;
		$this->xmlDocument->formatOutput = true;
		
		// Add people element to XML
		$people = $this->addElement('people',$this->xmlDocument);
		
		// Get array of persons
		$persons = $this->persons();
		
		// Loop persons
		foreach($persons as $person){
			
			// Add person element
			$childPerson = $this->addElement('person',$people);
			
			// Loop data from person
			foreach($person as $personData){
				
				// Explode PersonData by newline
				$rows = explode("\n", $personData);
				
				// Sort rows
				$rows = $this->sortOrder($rows);
		
				// Loop lines from PersonData and Family array
				foreach($rows as $id => $row){
					
					// explode row with pipe to array
					$columns = explode("|",substr($row,2));
					
					if(strpos($personData,'P|') !== false){
						
						if(substr($row,0,1) == 'P'){
							
							// Parse line that starts with P
							$dataP = $this->lineParser(['firstname','lastname'],$columns);
							
							// Add data from person
							foreach($dataP as $fieldname => $fieldvalue){
								$this->addElementTextNode($fieldname, $childPerson, $fieldvalue);
							}

						}else if(substr($row,0,1) == 'T'){
							
							// Parse line that starts with T
							$dataT = $this->lineParser(['mobile','landline'],$columns);
							
							// Add phone to person
							$childPhone = $this->addElement('phone',$childPerson);
							
							// Add data from phone
							foreach($dataT as $fieldname => $fieldvalue){
								$this->addElementTextNode($fieldname, $childPhone, $fieldvalue);
							}
							
						}else if(substr($row,0,1) == 'A'){
							
							// Parse line that starts with A							
							$dataA = $this->lineParser(['street','city','zipcode'],$columns);
							
							// Add address to person
							$childAddress = $this->addElement('address',$childPerson);
							
							// Add data from address
							foreach($dataA as $fieldname => $fieldvalue){
								$this->addElementTextNode($fieldname, $childAddress, $fieldvalue);
							}
						
						}
						
					}else if(strpos($personData,'F|') !== false){
						
						if(substr($row,0,1) == 'F'){ 
						
							// Parse line that starts with F
							$dataF = $this->lineParser(['name','born'],$columns);
							
							// Add family to person
							$childFamily = $this->addElement('family',$childPerson);
							
							// Add data from family
							foreach($dataF as $fieldname => $fieldvalue){
								$this->addElementTextNode($fieldname, $childFamily, $fieldvalue);
							}
							
						}else if(substr($row,0,1) == 'T'){
							
							// Parse line that starts with T
							$dataT = $this->lineParser(['mobile','landline'],$columns);	
							
							// Add phone to family
							$childPhone = $this->addElement('phone',$childFamily);
							
							// Add data from phone
							foreach($dataT as $fieldname => $fieldvalue){
								$this->addElementTextNode($fieldname, $childPhone, $fieldvalue);
							}
							
						}else if(substr($row,0,1) == 'A'){
							
							// Parse line that starts with A
							$dataA = $this->lineParser(['street','city','zipcode'],$columns);
							
							// Add address to family
							$childAddress = $this->addElement('address',$childFamily);
							
							// Add data from address
							foreach($dataA as $fieldname => $fieldvalue){
								$this->addElementTextNode($fieldname, $childAddress, $fieldvalue);
							}
							
						}
						
					}
					
				}
				
			}
			
		}
		
		
		// save XML and make it reachable 
		$this->xmlDocument->save("new_data.xml");
		
		return true;
		
	}
	
}
 
$dataConverter = new dataConverter();

// Print out XML
echo $dataConverter->xmlDocument->saveXML();